import React from 'react';
import {
  BrowserRouter,
  Route,
  Routes,
} from 'react-router-dom';
import Sidebar from './components/helpers/Sidebar';
import Home from './components/pages/Home';
import Film from './components/pages/Film';
import Films from './components/pages/Films';
import Characters from './components/pages/Characters';

function App() {
  return (
    <BrowserRouter>
      <Sidebar />
      <div className="ml-60">
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/movie" element={<Films />} />
          <Route path="/movie/:id" element={<Film />} />
          <Route path="/characters" element={<Characters />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
