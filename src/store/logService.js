function log(error) {
  console.error(error);
}

const logger = {
  log,
};
export default logger;
