import axios from 'axios';
import { React, useState, useEffect }  from 'react';
import { QueryClient, QueryClientProvider, useQuery } from 'react-query';

const limit = 1;
const apiKeyNum = "sXpGFDGZs0Dv1mmNFvYaGUvYwKX0PWIh";
const queryFilm = new QueryClient();

const Giphy = ({ search }) => {
  const [url, setGiphyt] = useState(null);
  const baseURL = `https://api.giphy.com/v1/gifs/search?q=${search}&api_key=${apiKeyNum}&limit=${limit}`;;

  useEffect(() => {
    axios.get(baseURL).then((response) => {
      setGiphyt(response.data);
    });
  }, []);
  
  if (!url || !url.data[0]) return null;

  return (
     <>
      <div className="flex flex-col mb-8">
          
          <img
            src={url.data[0].images.downsized.url}
            alt="132"
            className="w-full h-full object-center object-cover group-hover:opacity-75" />
      </div>
    </>
  );
};

export default function Wraped({search}) {
    return (
    <QueryClientProvider client={queryFilm}>
      <Giphy search={search}/>
    </QueryClientProvider>
    );
};