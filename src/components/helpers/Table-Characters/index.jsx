import React from 'react';
import { Link } from 'react-router-dom';

const TableCharacters = ({data}) => (
  <table className='table-auto '>
    <thead className='bg-gray-50 dark:bg-gray-700'>
      <tr>
        <th className='py-3 px-6 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400'>Name</th>
        <th className='py-3 px-6 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400'>Age</th>
        <th className='py-3 px-6 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400'>Gender</th>
        <th className='py-3 px-6 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400'>Hair color</th>
        <th className='py-3 px-6 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400'>Eye color</th>
        <th className='py-3 px-6 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400'>Film</th>
      </tr>
    </thead>
    <tbody>
      {
        data.map((  { id, name, age, gender, hair_color, eye_color, films }) => {
          const movie_url = films[0].split('/').pop();
          return (
              <tr key={id} className='border-b odd:bg-white even:bg-gray-50 odd:dark:bg-gray-800 even:dark:bg-gray-700 dark:border-gray-600'>
                  <td className='py-4 px-6 text-sm font-medium text-gray-900 whitespace-nowrap dark:text-white'>{name}</td>
                  <td className='py-4 px-6 text-sm text-gray-500 whitespace-nowrap dark:text-gray-400'>{age}</td>
                  <td className='py-4 px-6 text-sm text-gray-500 whitespace-nowrap dark:text-gray-400'>{gender}</td>
                  <td className='py-4 px-6 text-sm text-gray-500 whitespace-nowrap dark:text-gray-400'>{hair_color}</td>
                  <td className='py-4 px-6 text-sm text-gray-500 whitespace-nowrap dark:text-gray-400'>{eye_color}</td>
                  <td className='py-4 px-6 text-sm text-gray-500 whitespace-nowrap dark:text-gray-400'><Link to={`/movie/${movie_url}`}>Movie</Link></td>
              </tr>
          );
        })
      }
    </tbody>
  </table>
)

export default TableCharacters;
