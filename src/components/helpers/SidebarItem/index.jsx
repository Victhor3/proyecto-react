import React from 'react';
import { Link } from 'react-router-dom';

const SidebarItem = ({ route, text }) => (
  <Link to={route} className="flex py-1 px-4 rounded text-md items-center content-center justify-center md:items-start md:content-start md:justify-start transition duration-200 hover:bg-primary-200">
    <span className="text-lg mt-2 md:mt-0">{text}</span>
  </Link>
);

export default SidebarItem;
