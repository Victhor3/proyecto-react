import React, { useState } from 'react';
import Giphy from '../Giphy';
import axios from 'axios';

const CardCharacter = ({ id, name, age, gender, hair_color, eye_color, films  }) => {
  const [film, setFilm] = useState();
  const movie_url = films[0].split('/').pop();
  axios.get(`https://ghibliapi.herokuapp.com/films/${movie_url}`).then(res => {
    setFilm(res.data.title);
  });

  if (!film) return null;

  return (
    <>
      <div className="flex flex-col border-2 p-4 rounded-lg" role="button">
        <div className="cat w-full aspect-w-1 aspect-h-1 bg-gray-150 rounded-lg overflow-hidden xl:aspect-w-7 xl:aspect-h-8">
          <Giphy search={film}></Giphy>
        </div>
        <div className="flex flex-col mb-8">
          <h3 className="mt-4 text-md text-gray-700 text-center">Name<br />{name}</h3>
          <label className="text-lg pt-2 text-center">Age<br />{age}</label>
          <label className="text-lg pt-2 text-center">Gender<br />{gender}</label>
          <label className="text-lg pt-2 text-center">Hair Color<br />{hair_color}</label>
          <label className="text-lg pt-2 text-center">Eye Color<br />{eye_color}</label>
          <label className="text-lg pt-2 text-center">Movie<br />{film}</label>
        </div>
      </div>
    </>
  );
};

export default CardCharacter;
