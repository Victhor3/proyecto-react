import React, { useState } from 'react';
import Modal from '../Modal';
import Film from '../../pages/Film';

const CardPelicula = ({ id, image, description, title, original_title }) => {
  const [showModal, setShowModal] = useState(false);
  return (
    <>
      <div className="flex flex-col border-2 p-4 rounded-lg" role="button" onClick={() => setShowModal(!showModal)} onKeyDown={() => setShowModal(!showModal)}>
        <div className="cat w-full aspect-w-1 aspect-h-1 bg-gray-200 rounded-lg overflow-hidden xl:aspect-w-7 xl:aspect-h-8">
          <img
            src={image}
            alt={description}
            className="w-full h-full object-center object-cover group-hover:opacity-75" />
        </div>
        <h3 className="mt-4 text-sm text-gray-700 text-center">{title}</h3>
        <p className="mt-1 text-lg font-medium text-gray-900 text-center">{original_title}</p>
      </div>
      <Modal show={showModal}
        onClickClose={() => setShowModal(false)}
        title={title}>
        <Film id={id} />
      </Modal>
    </>
  );
};

export default CardPelicula;
