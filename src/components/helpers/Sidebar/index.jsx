import React from 'react';
import { Link } from 'react-router-dom';
import SidebarItem from '../SidebarItem';
import Logo from '../../../assets/images/Logo_Ghilbi.webp';

const Sidebar = () => (
  <div className="relative max-w-6xl mx-auto px-4 flex-auto">
    <div className="sidebar fixed z-10 min-h-screen bg-[#fae3d6] text-gray-800 w-60 space-y-8 py-1 px-2inset-y-0 left-0 transform -translate-x-full  md:translate-x-0 transition duration-200 ease-in-out">
      <Link to="/" className="text-white flex items-center px-4 justify-center">
        <img src={Logo} alt="Logo" />
      </Link>
      <nav>
        <SidebarItem route="/" text="Home" />
        <SidebarItem route="/movie" text="Movies" />
        <SidebarItem route="/characters" text="Characters" />
      </nav>
    </div>
  </div>
);

export default Sidebar;
