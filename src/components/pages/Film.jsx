import React, { useState, useEffect }  from 'react';
import { QueryClient, QueryClientProvider, useQuery } from 'react-query';
import http from '../../store/httpService';
import Loading from '../helpers/Loading';

const queryFilm = new QueryClient();

const Film = ({ id }) => {
  const [ film, setFilm ] = useState([]);
  const { title, original_title, description, director, movie_banner, producer, release_date, rt_score } = film;
  const { isLoading, data } = useQuery('getFilm', async () => {
    try {
      const { data } = await http.get(`films/${id}`);
      return data;
    } catch (ex) {
      return false;
    }
  });

  useEffect(() => {
    if (data) {
      setFilm(data)
    }
  }, [data]);

  return (
    <>
      {isLoading ? <Loading/> :
        <div className="flex flex-col mb-8">
          <img src={movie_banner} alt={title} className="h-[30rem] object-cover shadow-2xl rounded-md" />
            <label className="text-2xl pt-12 text-center">{title}</label>
            <label className="text-lg text-center">{original_title}</label>
            <label className="text-sm text-center">{director}, {producer} - {release_date}</label>
            <label className="text-sm text-center">Score: {rt_score}</label>
            <label className="text-lg text-center"> {description} </label>
          </div>
        }
    </>
    );
};

export default function Wraped({id}) {
  return (
  <QueryClientProvider client={queryFilm}>
    <Film id={id} />
  </QueryClientProvider>
  );
};
