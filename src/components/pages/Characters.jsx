import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { QueryClient, QueryClientProvider, useQuery } from 'react-query';
import Loading from '../helpers/Loading';
import http from '../../store/httpService';
import CardCharacter from '../helpers/Card-Character';

const queryCharacter = new QueryClient();
const Characters = () => {
  const navigate = useNavigate();
  const [people, setPeople] = useState([]);

  const { isLoading, data } = useQuery('getPeople', async () => {
    try {
      const { data } = await http.get('people');
      return data;
    } catch (ex) {
      if (ex.response && ex.response.status === 404) {
        navigate('/not-found');
      }
      return false;
    }
  });

  useEffect(() => {
    if (data) {
      setPeople(data);
      console.log(data);
    }
  }, [data]);

  return (
    <div className="bg-white p-4">
      <div className="card-size card-margen">
        <h2 className="sr-only">Products</h2>
        {
          isLoading ? <Loading />
          :
            <div className="grid grid-cols-1 gap-y-10 sm:grid-cols-2 gap-x-6 lg:grid-cols-3 xl:grid-cols-4 xl:gap-x-8">
              {people.map((people) => (
                <CardCharacter {...people} key={people.id} />
              ))}
            </div>
        }
      </div>
    </div>
  );
};

export default function Wraped() {
  return (
    <QueryClientProvider client={queryCharacter}>
      <Characters />
    </QueryClientProvider>
  );
}
