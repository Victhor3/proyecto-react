/* eslint-disable react/jsx-props-no-spreading */
import { React, useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { QueryClient, QueryClientProvider, useQuery } from 'react-query';
import Loading from '../helpers/Loading/';
import http from '../../store/httpService';
import CardPelicula from '../helpers/Card-Pelicula';

const queryFilms = new QueryClient();

function Films() {
  const navigate = useNavigate();
  const [films, setFilms] = useState([]);

  const { isLoading, data } = useQuery('getFilms', async () => {
    try {
      const { data } = await http.get('films');
      return data;
    } catch (ex) {
      if (ex.response && ex.response.status === 404) {
        navigate('/not-found');
      }
    }
  });

  useEffect(() => {
    if (data) {
      setFilms(data);
    }
  }, [data]);

  return (
    <div className="bg-white p-4">
      <div className="card-size card-margen">
        <h2 className="sr-only">Products</h2>
        {
          isLoading ? <Loading />
            : <div className="grid grid-cols-1 gap-y-10 sm:grid-cols-2 gap-x-6 lg:grid-cols-3 xl:grid-cols-4 xl:gap-x-8">
              {films.map((product) => (
                <CardPelicula {...product} key={product.id} />
              ))}
            </div>
        }
      </div>
    </div>
  );
}

export default function Wraped() {
  return (
    <QueryClientProvider client={queryFilms}>
      <Films />
    </QueryClientProvider>
  );
}
