import React from 'react';
import BackgroundGhilbi from '../../assets/images/bg-ghilbi.jpg';

const Home = () => {
  return (
    <div className="container flex flex-col h-[59rem] bg-fixed bg-clip-padding bg-center" style={{ backgroundImage: `url(${BackgroundGhilbi})` }}>
      <div className="bg-fixed bg-white m-12 mt-auto h-96 w-auto opacity-80 flex flex-col">
        <p className="text-center text-3xl font-semibold text-gray-700 mb-12">Welcome to Ghilbi Fan-Site</p>
        <p className="text-gray-600 text-xl text-center">Here you can find information about the Ghilbi Studios movies, and their characters</p>
        <p className="text-gray-600 text-xl text-center">Please refer to the sidebar menu, to move through the website</p>
      </div>
    </div>
  );
};

export default Home;
