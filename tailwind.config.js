module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      'bg-sidebar': '#E9B193'
    },
  },
  plugins: [],
}