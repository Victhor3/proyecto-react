module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    'plugin:react/recommended',
    'airbnb',
  ],
  parserOptions: {
    ecmaFeatures: {
      jsx: false,
    },
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  plugins: [
    'react',
  ],
  rules: {
    'camelcase': 'off',
    "react/prop-types": "off",
    "arrow-body-style": "off",
    "react/jsx-filename-extension": [
      1, { "extensions": [".js", ".jsx"] }
    ],
    indent: ['error', 2, { "MemberExpression": 1 }]
  },
};
